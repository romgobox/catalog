$(document).ready(function() {
    // Создаем экземпляры классов продуктов и корзины
    var products = new Products();
    window.products = products;

    var basket = new Basket();
    window.basket = basket;

    // Загружаем товары, категории и тэги
    products.getProducts();
    products.getCategories();
    products.getTags();

    // Выводим в интерфейс список категорий, все товары и список тэгов
    products.renderCategoriesList();
    products.renderCategoryItems(0);
    products.renderTags();

    // Изменение отображения списка товаров
    $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
    $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});

    // Обработка нажатия фильтра тэгов
    $("#tags").on('click', "#tags_filter", function (e){
        e.preventDefault();
        products.filterByTags();
    });

    //Обработка выбора категории
    $("#categories").on('click', ".category_item", function (e){
        e.preventDefault();
        var category_id = this.id;
        products.renderCategoryItems(category_id);
    });

    // Обработка нажатия кнопки В корзину
    $("#products").on('click', ".addToCart", function (e){
        e.preventDefault();
        var product = products.products[parseInt(this.id)];
        addProductDialog(product);
    });

    // Обработка нажатия кнопки Корзина
    $("#navbar").on('click', "#basket", function (e){
        e.preventDefault();
        basket.showItems();
    });
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};

function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            csrftoken = getCookie('csrftoken');
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
