var Product = function(data) {
    // Класс продукта
    this.id = data['id'];
    this.title = data['title'];
    this.description = data['description'];
    this.price = data['price'];
    this.tags = data['tags'];
    this.category_id = data['category_id'];
}

var Tag = function(data) {
    // Класс тэга
    this.name = data['name'];
}

var Category = function(data) {
    // Класс категории
    this.id = data['id'];
    this.name = data['name'];

}

var Products = function() {
    // Класс, содержащий продукты, категории, тэги для фильтрации и сами тэги
    this.products = {};
    this.categories = {};
    this.filterTags = [];
    this.tags = [];
}

Products.prototype.addTag = function(tag) {
    // Метод добавляет тэг в класс продуктов
    this.tags.push(tag);
}

Products.prototype.getTags = function() {
    // Метод для загрузки тэгов
    var self = this;
    $.ajax({
        url: "tags/",
        type: "GET",
        async: false,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
                        $.each(data, function(index, item) {
                            self.addTag(new Tag(item));
                        });
                    }
    });
}

Products.prototype.renderTags = function () {
    // Метод для вывода списка тэгов
    var tags = this.tags;
    var html = '';
    for (var i=0; i<tags.length; i++) {
        html += '<label class="btn btn-default">';
        html += '<input type="checkbox" name="tag" value="'+tags[i]['name']+'">'+tags[i]['name'];
        html += '</label>';
    }
    html += '<input type="button" id="tags_filter" class="btn btn-success" value="Отфильтровать">';
    $("#tags").html(html);
}

Products.prototype.filterByTags = function () {
    // Метод фильтрации тэгов
    var self = this;
    self.filterTags = [];
    var filterProducts = [];
    $('#tags input[type="checkbox"]:checked').each(function() {
            self.filterTags.push(($(this).val()));
    });
    $.each(self.products, function(index, item) {
        for (var i=0; i<item['tags'].length; i++) {
            if ($.inArray(item['tags'][i], self.filterTags) != -1) {
                if ($.inArray(item, filterProducts) == -1) {
                    filterProducts.push(item);
                }
            }
        }
    });
    var html = '';
    for (i=0; i<filterProducts.length; i++) {
        html += self.renderProductItem(filterProducts[i]['id']);
    }
    if (filterProducts.length == 0) {
        self.renderCategoryItems(0);
    }
    else {
        $("#products").html(html);
    }
}

Products.prototype.renderProductItem = function (id) {
    // Метод для вывода продукта
    var self = this;
    var html = '';
    $.each(self.products, function(index, item) {
        if (item['id'] == id) {
            html += '<div class="item  col-xs-4 col-lg-4">';
            html += '<div class="thumbnail">';
            html += '<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />';
            html += '<div class="caption">';
            html += '<h4 class="group inner list-group-item-heading">'+item['title']+'</h4>';
            html += '<p class="group inner list-group-item-text">'+item['description']+'</p>';
            html += '<p class="group inner list-group-item-text">';
            html += 'Тэги: ';
            for (var i=0; i<item['tags'].length; i++) {
                html += item['tags'][i]+' ';
            }
            html += '</p>';
            html += '<div class="row"><div class="col-xs-12 col-md-6">';
            html += '<p class="lead">'+item['price']+' $</p></div>';
            html += '<a id="'+item['id']+'" class="addToCart btn btn-success" href="#">В корзину</a>';
            html += '</div></div></div></div></div>';
        }
    });
    return html;
}

Products.prototype.renderCategoryItems = function (id) {
    // Метод для вывода продуктов определенной категории
    var self = this;
    var html = '';
    $.each(self.products, function(index, item) {
        if (item['category_id'] == id || id == 0) {
            html += self.renderProductItem(item['id']);
        }
    });
    $("#products").html(html);
}

Products.prototype.renderCategoriesList = function () {
    // Метод для вывода списка категорий
    var self = this;
    var html = '<li id="0" class="category_item"><a href="#">Все</a></li>';
    $.each(self.categories, function(index, item) {
        html += '<li id='+item['id']+' class="category_item"><a href="#">'+item['name']+'</a></li>';
    });
    $("#categories").html(html);
}

Products.prototype.addProduct = function(product) {
    // Метод для добавления продукта в класс продуктов
    this.products[product['id']] = product;
}

Products.prototype.getProducts = function() {
    // Метод для загрузки продуктов
    var self = this;
    $.ajax({
        url: "products/",
        type: "GET",
        async: false,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
                        $.each(data, function(index, item) {
                            self.addProduct(new Product(item));
                        });
                    }
    });
}


Products.prototype.addCategory = function (category) {
    // Метод для добавления категории в класс продуктов
    this.categories[category['id']] = category;
}

Products.prototype.getCategories = function() {
    // Метод для загрузки категорий
    var self = this;
    $.ajax({
        url: "categories/",
        type: "GET",
        async: false,
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
                        $.each(data, function(index, item) {
                            self.addCategory(new Category(item));
                        });
                    }
    });
}