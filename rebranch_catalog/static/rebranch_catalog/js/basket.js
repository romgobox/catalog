var Basket = function() {
    // Класс корзины
    this.products = {};
};

Basket.prototype.addProduct = function(product, count) {
    // Метод для добавления товара в корзину
    var count = parseInt(count);
    if (this.products) {
        if (!this.products[product['id']]) {
            this.products[product['id']] = {'data':product, 'count':count};
        }
        else {
            this.products[product['id']]['count'] += count;
        }
    }
    else {
        this.products[product['id']] = {'data':product, 'count':count};
    }
}
Basket.prototype.totalSum = function() {
    // Метод для подсчета общей суммы товаров в корзине
    var sum = 0;
    $.each(this.products, function(index, item) {
        sum = sum + item['data']['price'] * item['count'];
    });
    return sum;
}
Basket.prototype.showItems = function() {
    // Метод для вывода диалога корзины
    var self = this;
    var html = '<table class="table table-bordered table-hover">';
    html += '<tr><td>Наименование</td><td>Цена</td><td>Количество</td></tr>'
    $.each(this.products, function(index, item) {
        html += '<tr><td>'+item['data']['title']+'</td>';
        html += '<td>'+item['data']['price']+'</td>';
        html += '<td>'+item['count']+'</td></tr>';
    });
    html += '<tr><td>Итоговая сумма</td><td>'+this.totalSum()+'</td></tr>';
    html += '<tr><td>Куда доставить</td><td><input type="text" id="address"></td></tr>';
    html += '</table>';
    $("#basket_dialog").html(html);
    $("#basket_dialog").dialog({
        title: 'Корзина',
        height: 600,
        width: 600,
        modal: true,
        resizable: true,
        buttons: [
            {
                text: "Оформить заказ",
            click: function () {
                    var address = $("#address").val();
                    self.completeOrder(address);
                    $(this).dialog("close");
                }
            }
          ]
    });
}

Basket.prototype.completeOrder = function(address) {
    // Метод для оформления заказа
    var self = this;
    var data = {};
    data['products'] = self.products;
    data['total_sum'] = self.totalSum();
    data['address'] = address;
    data["csrfmiddlewaretoken"] = getCookie('csrftoken');
    $.ajax({
        url: "complete_order/",
        type: "POST",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
                        self.products = {};
                        orderCompleteStatus(data['status']);
                    }
    });
}

function orderCompleteStatus(status) {
    // Функция выводит окно с сообщением о добавлении заказа
    $("#order_status_dialog").html(status);
    $("#order_status_dialog").dialog({
        title: 'Заказ',
        height: 200,
        width: 400,
        modal: true,
        resizable: true,
        buttons: [
            {
                text: "Ok",
            click: function () {
                    $(this).dialog("close");
                }
            }
          ]
    });
}

function addProductDialog(product) {
    // Функция выводит диалог добавления товара в корзину
    var self = this;
    var product = product;
    var html = '<table id="product_dialog" class="table table-bordered table-hover">';
    html += '<tr><td>Наименование</td><td>Цена</td><td>Количество</td></tr>'
    html += '<tr><td>'+product['title']+'</td>';
    html += '<td>'+product['price']+'</td>';
    html += '<td><input type="text" id="product_count" value=1></td></tr>';
    html += '</table>';
    $("#add_product_dialog").html(html);
    $("#add_product_dialog").dialog({
        title: 'Добавляем в корзину',
        height: 300,
        width: 600,
        modal: true,
        resizable: true,
        buttons: [
            {
                text: "Добавить в корзину",
            click: function () {
                    var count = $("#product_count").val();
                    if (count<=0) {
                        count = 1;
                    }
                    basket.addProduct(product, count);
                    $(this).dialog("close");
                }
            }
          ]
    });
}
