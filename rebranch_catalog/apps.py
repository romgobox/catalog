from __future__ import unicode_literals

from django.apps import AppConfig


class RebranchCatalogConfig(AppConfig):
    name = 'rebranch_catalog'
