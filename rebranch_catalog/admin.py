# -*- coding: utf-8 -*-
from django.contrib import admin
from django.http import HttpResponse
from django.contrib.auth.models import User


from .models import Categories, Products, Orders, OrderDetail

def export_orders_details_csv(modeladmin, request, queryset):
    import csv
    from django.utils.encoding import smart_str
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=orders.csv'
    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8'))
    writer.writerow([
        smart_str(u"Заказ №"),
        smart_str(u"Пользователь"),
        smart_str(u"Общая стоимость"),
    ])
    for obj in queryset:
        writer.writerow([
            smart_str(obj.id),
            smart_str(obj.user.username),
            smart_str(obj.total_sum),
        ])
    return response
export_orders_details_csv.short_description = u"Export CSV"

class OrdersAdmin(admin.ModelAdmin):
    actions = [export_orders_details_csv]

admin.site.register(Categories)
admin.site.register(Products)
admin.site.register(Orders, OrdersAdmin)
admin.site.register(OrderDetail)