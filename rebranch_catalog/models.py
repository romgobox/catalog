# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

from taggit.managers import TaggableManager

class Categories(models.Model):
    name = models.CharField(max_length=200)

    def __unicode__(self):
        return self.name

    def to_dict(self):

        return {'id': self.id,
                'name': self.name,}

    class Meta:
        verbose_name_plural = u"Категории"

class Products(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    price = models.FloatField()
    tags = TaggableManager()
    category = models.ForeignKey(Categories)

    def __unicode__(self):
        return self.title

    def to_dict(self):

        return {'id': self.id,
                'title': self.title,
                'description': self.description,
                'price': self.price,
                'tags': list(self.tags.names()),
                'category_id':self.category.id}

    class Meta:
        verbose_name_plural = u"Товары"

class Orders(models.Model):
    user = models.ForeignKey(User)
    address = models.CharField(max_length=300)
    total_sum = models.FloatField()

    def __unicode__(self):
        return self.user.username + ' ' + self.address

    class Meta:
        verbose_name_plural = u"Заказы"



class OrderDetail(models.Model):
    order = models.ForeignKey(Orders)
    product = models.ForeignKey(Products)
    quantity = models.IntegerField()

    def __unicode__(self):
        return u'Заказ № ' + str(self.order.id)

    class Meta:
        verbose_name_plural = u"Детали заказов"