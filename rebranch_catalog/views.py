# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.template import loader
from django.contrib.auth.models import User
import json

from django.shortcuts import render_to_response
from django.contrib.auth.forms import UserCreationForm
from django.core.context_processors import csrf

from .models import Categories, Products, Orders, OrderDetail
from taggit.models import Tag

def index(request):
    template = loader.get_template('rebranch_catalog/index.html')
    user = None
    if request.user.username:
        user = request.user
    context = {
        'user': user,
    }
    return HttpResponse(template.render(context, request))

def rebranch_catalog(request):
    template = loader.get_template('rebranch_catalog/rebranch_catalog.html')
    user = None
    if request.user.username:
        user = request.user
    context = {
        'user': user,
    }
    return HttpResponse(template.render(context, request))

def products(request):
    products = Products.objects.order_by('category')
    products_list = []
    for product in products:
        products_list.append(product.to_dict())
    return JsonResponse(products_list, safe=False)

def categories(request):
    categories = Categories.objects.order_by('id')
    categories_list = []
    for category in categories:
        categories_list.append(category.to_dict())
    return JsonResponse(categories_list, safe=False)

def tags(request):
    tags = Tag.objects.all()
    tags_list = []
    for tag in tags:
        tags_list.append({'name':tag.name})
    return JsonResponse(tags_list, safe=False)

def register(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/accounts/register/complete')

    else:
        form = UserCreationForm()
    token = {}
    token.update(csrf(request))
    token['form'] = form

    template = loader.get_template('registration/registration_form.html')

    return HttpResponse(template.render(token, request))

def registration_complete(request):
    return HttpResponseRedirect('/accounts/login/')

def complete_order(request):
    if request.method == 'POST':
        order_data = json.loads(request.body)
        user_id = request.user.id
        user = User.objects.get(id=user_id)
        order = Orders(user=user, address=order_data['address'], total_sum=order_data['total_sum'])
        order.save()
        for key, value in order_data['products'].items():
            product = Products.objects.get(id=int(key))
            order_detail = OrderDetail(order=order, product=product, quantity=value['count'])
            order_detail.save()
        return JsonResponse({'status':u'Заказ оформлен!'}, safe=False)