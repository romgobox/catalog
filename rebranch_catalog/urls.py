from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.rebranch_catalog, name='rebranch_catalog'),
    url(r'^products/', views.products, name='products'),
    url(r'^categories/', views.categories, name='categories'),
    url(r'^tags/', views.tags, name='tags'),
    url(r'^complete_order/', views.complete_order, name='complete_order'),
]